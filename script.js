document.addEventListener('DOMContentLoaded', event => {
    if (localStorage.getItem('cinema') != null) {
        var cinema = [
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
        ];
        localStorage.setItem('cinema', JSON.stringify(cinema));
    }
    var cinema = JSON.parse(localStorage.getItem('cinema'));
    createLayout(cinema);
    const container = document.querySelector('.seat-container');
    container.addEventListener('click', event => {
        if (event.target.classList.contains('seat') && !event.target.classList.contains('occupied')) {
            event.target.classList.toggle('selected');
            updateLayout();
        }
    });
    const button = document.querySelector('.btn');
    button.addEventListener('click', event => {
        const available = container.querySelectorAll('.seat');
        const selected = container.querySelectorAll('.seat.selected');
        const seatindex = [...selected].map(seat => [...available].indexOf(seat));
        seatindex.forEach(index => {
            let row = Math.floor(index / cinema[0].length);
            let col = index % cinema[0].length;
            cinema[row][col] = true;
        });
        selected.forEach(element => {
            element.classList.toggle('selected');
            element.classList.toggle('occupied');
        });
        updateLayout();
        localStorage.setItem('cinema', JSON.stringify(cinema));
    });
});

function createLayout(cinema) {
    const container = document.querySelector('.seat-container');
    cinema.forEach((row, index) => {
        const rowcontainer = document.createElement('div');
        rowcontainer.classList.add('row');
        const letter = document.createElement('span');
        letter.classList.add('letter');
        letter.innerText = `${String.fromCharCode((64 + cinema.length) - index)}`
        rowcontainer.appendChild(letter)
        row.forEach((column, index) => {
            const seat = document.createElement('div');
            seat.classList.add('seat');
            seat.innerText = `${index + 1}`
            if (column) seat.classList.add('occupied');
            rowcontainer.appendChild(seat);
        })
        container.appendChild(rowcontainer);
    });
}

function updateLayout() {
    const container = document.querySelector('.seat-container');
    const selected = container.querySelectorAll('.seat.selected');
    document.getElementById('count').innerText = selected.length;
    if (selected.length != 0) {
        document.getElementById('total').innerText = selected.length * (40000 + 1000);
    } else {
        document.getElementById('total').innerText = selected.length;
    }
}